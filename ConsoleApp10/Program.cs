﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideProov
{
    class Program
    {
        static void Main(string[] args)
        {
            Õppeaine Aine1 = new Õppeaine("Matemaatika", 20);
            Õppeaine Aine2 = new Õppeaine("Füüsika", 20);
            Õppeaine Aine3 = new Õppeaine("Keemia", 40);
            Õppeaine Aine4 = new Õppeaine("Emakeel", 60);
            Õppeaine Aine5 = new Õppeaine("Kehaline kasvatus", 100);

            Õpilane õ1 = new Õpilane("Mari", "Toomingas", 2010);
            Õpilane õ2 = new Õpilane("Jüri", "Toomingas", 2012);
            Õpilane õ3 = new Õpilane("Malle", "Tamm", 2012);
            Õpilane õ4 = new Õpilane("Kalle", "Kask", 2015);
            Õpilane õ5 = new Õpilane("Mari", "Org", 2017);

            Õpetaja õp1 = new Õpetaja("Henn", "Sarv", 1955);
            Õpetaja õp2 = new Õpetaja("Juta", "Sarapik", 1955);
            Õpetaja õp3 = new Õpetaja("Mati", "Öövel", 1955);

            Klass kl1a = new Klass("1a");
            Klass kl1b = new Klass("1b");

            Console.WriteLine("Õppeained:"); foreach (var x in Õppeaine.Õppeained) Console.WriteLine(x);
            Console.WriteLine("Õpilased:"); foreach (var y in Õpilane.Õpilased) Console.WriteLine(y);
            Console.WriteLine("Õpetajad:"); foreach (var z in Õpetaja.Õpetajad) Console.WriteLine(z);
            Console.WriteLine("Klassid:"); foreach (var w in Klass.Klassid) Console.WriteLine(w);
        }
    }

    class Inimene
    {
        string _EesNimi;
        string _PereNimi;
        int _SünniAasta;
        public string EesNimi { get { return _EesNimi.Substring(0, 1).ToUpper() + _EesNimi.Substring(1).ToLower(); } }
        public string PereNimi { get { return _PereNimi.Substring(0, 1).ToUpper() + _PereNimi.Substring(1).ToLower(); } }
        public int SünniAasta { get { return _SünniAasta; } }

        public Inimene ( string eesNimi, string pereNimi, int sünniAasta)
        {
            _EesNimi = eesNimi;
            _PereNimi = pereNimi;
            _SünniAasta = sünniAasta;
        }
        public override string ToString()
        {
            return $"{EesNimi} {PereNimi}, sündinud {SünniAasta}";
        }
    }

    class Õpilane : Inimene
    {
        public static List<Õpilane> Õpilased = new List<Õpilane>();
        public Klass ÕpibKlassis = null;

        public Õpilane(string eesNimi, string pereNimi, int sünniAasta) : base (eesNimi,pereNimi,sünniAasta)
        {
            Õpilased.Add(this);
        }
        public Õpilane(string eesNimi, string pereNimi, Klass klass) : this(eesNimi,pereNimi,0)         // seda praegu üldse kunagi vaja ei ole, aga kui oleks, kas siis tekib laps sünniaastaga 0 ???
        { this.ÕpibKlassis = klass; klass.õpilasteNimekiri.Add(this); }
    }

    class Õpetaja : Inimene
    {
        public static List<Õpetaja> Õpetajad = new List<Õpetaja>();
        public Õppeaine ÕpetabAinet = null;

        public Õpetaja ( string eesNimi, string pereNimi, int sünniAasta) : base (eesNimi,pereNimi,sünniAasta)
        {
            Õpetajad.Add(this);
        }
    }

    class Õppeaine
    {
        public string aineNimi;
        public int tunde;
        public static List<Õppeaine> Õppeained = new List<Õppeaine>();

        public Õppeaine (string aineNimi, int tunde)            // konstruktor, uue Õppeaine loomine
        {
            this.aineNimi = aineNimi;
            this.tunde = tunde;
            Õppeained.Add(this);
        }

        public override string ToString()
        {
            return $"õppeaine {aineNimi}, {tunde} tundi";
        }
    }

    class Klass
    {
        internal static List<Klass> Klassid = new List<Klass>();
        internal List<Õpilane> õpilasteNimekiri = new List<Õpilane>();
        string _klassiNimi;
        public string KlassiNimi { get { return _klassiNimi; } }
        public Klass(string klassiNimi) { _klassiNimi = klassiNimi; Klassid.Add(this); }
        public void UusÕpilane (string eesNimi, string pereNimi, int sünniaasta) { new Õpilane(eesNimi, pereNimi, sünniaasta); }
        public override string ToString()
        {
            return $"klass {KlassiNimi}, {õpilasteNimekiri.Count} õpilast";
        }
    }
}
